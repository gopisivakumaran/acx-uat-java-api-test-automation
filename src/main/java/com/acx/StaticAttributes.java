package com.acx;
        
import java.time.Instant;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.assetcontrol.acx.api.model.Ado;
import com.assetcontrol.acx.api.query.Query;
import com.assetcontrol.acx.streaming.client.StreamingClient;
import com.assetcontrol.acx.streaming.client.impl.RangeImpl;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Iterator;

import com.assetcontrol.acx.api.model.TimeSeries;
import com.assetcontrol.acx.api.model.TimeSeriesRecord;
import com.assetcontrol.acx.api.query.Results;

public class StaticAttributes {

	public static final String SERVERS = "35.195.54.37:50051";

	public static void main(String[] args) throws ParseException {
		Properties properties = new Properties();
        	properties.setProperty("servers", SERVERS);	

		StreamingClient client = new StreamingClient(properties);
        	client.initialize();
		client.login("admin","adminpass");		

		if (args.length == 1){
		Query<Ado> ados = client.connection().createAdoQuery(
                        Arrays.asList(args[0]),
			Arrays.asList(),                      
                        Instant.now(),
                        Instant.now());	
		ados.getResults().forEach(ado -> ado.getAttributes().forEach(attr ->
                System.out.println(attr.getId() + " : " + ado.getAttributeValue(attr))));
		}
 
		else if (args.length == 2){
		Query<Ado> ados = client.connection().createAdoQuery(
                        Arrays.asList(args[0]),
                        Arrays.asList(args[1]),
                        Instant.now(),
                        Instant.now());
		ados.getResults().forEach(ado -> ado.getAttributes().forEach(attr ->
                System.out.println(attr.getId() + " : " + ado.getAttributeValue(attr))));
                }
	}
}


