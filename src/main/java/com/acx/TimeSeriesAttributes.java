package com.acx;
        
import java.time.Instant;
import java.util.Arrays;
import java.util.Properties;

import com.assetcontrol.acx.api.model.Ado;
import com.assetcontrol.acx.api.query.Query;
import com.assetcontrol.acx.streaming.client.StreamingClient;
import com.assetcontrol.acx.streaming.client.impl.RangeImpl;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Iterator;

import com.assetcontrol.acx.api.model.TimeSeries;
import com.assetcontrol.acx.api.model.TimeSeriesRecord;
import com.assetcontrol.acx.api.query.Results;


public class TimeSeriesAttributes {

	public static final String SERVERS = "35.195.54.37:50051";

	public static void main(String[] args) throws ParseException {
		Properties properties = new Properties();
        	properties.setProperty("servers", SERVERS);	

		StreamingClient client = new StreamingClient(properties);
        	client.initialize();
		client.login("admin","adminpass");	

		Instant startDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2018-06-18 00:00:00").toInstant();
                Instant endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2018-06-19 00:00:00").toInstant();

		Results<TimeSeries> results = client.connection().createTimeSeriesQuery(

		Arrays.asList("C0.ISS.ACX.100000"), "CONSOLIDATION_C0",
		new RangeImpl(startDate, endDate),
		Instant.now()).getResults();

		Iterator<TimeSeries> iter = results.iterator();

		TimeSeries ts = iter.next();

		for (TimeSeriesRecord rec : ts.getRecords()) {
			rec.getAttributes().forEach(attr -> {
				System.out.println(rec.getAsOf());
			});
			rec.getAttributes().forEach(attr -> {
				System.out.println(attr.getId());
			});
			rec.getAttributes().forEach(attr -> {
				System.out.println(rec.getAttributeValue(attr));
			});

			rec.getAttributes().forEach(attr -> {
                                System.out.println(rec.getAsOn());
                        });
		}
	}
}
