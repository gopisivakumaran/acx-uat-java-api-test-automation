package com.acx;
        
import java.time.Instant;
import java.util.Arrays;
import java.util.Properties;
import java.util.List;
import java.util.ArrayList;

import com.assetcontrol.acx.api.model.Ado;
import com.assetcontrol.acx.api.query.Query;
import com.assetcontrol.acx.streaming.client.StreamingClient;
import com.assetcontrol.acx.streaming.client.impl.RangeImpl;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Iterator;
import com.assetcontrol.acx.api.query.Results;

import java.io.*;

public class AdoBrowseSingle {

        public static final String SERVERS = "35.195.54.37:50051";

        public static void main(String[] args) throws ParseException, IOException {
                
		Properties properties = new Properties();
                properties.setProperty("servers", SERVERS);

                StreamingClient client = new StreamingClient(properties);
                client.initialize();
		client.login("admin","adminpass");

		String str = args[0];
	        Results<String> results = client.connection().createAdoBrowseQuery(
        	"C0_IS093 = \"" + str + "\"", null, Instant.now(), Instant.now()).getResults();

                List<String> list = new ArrayList<>();
                int limit = 5;
                Iterator<String> symbols = results.iterator();

                for (int i = 0; i < limit ; i++) {
                        if (symbols.hasNext()) {
                                list.add(symbols.next());
                        }
                }

                Query<Ado> ados = client.connection().createAdoQuery(
                        list,
                        null,
                        Instant.now(),
                        Instant.now());
                ados.getResults().forEach(ado -> System.out.println(ado.getId()));
        }

}

