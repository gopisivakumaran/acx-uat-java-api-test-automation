PROGNAME=`basename $0`
logfn=`date +'%Y''%m%d_%H:%M:%S'`.log

info(){
	echo -e "+++INFO+++: `date +'%Y''%m%d_%H:%M:%S'` $PROGNAME: $1" | tee -a log/$logfn
}

success(){
if [ $? -eq 0 ]
then
        info "Success: $1"
else
        info "Failure: $1"
        exit 1
fi
}

pass(){
        echo -e "\e[0;32m+++PASS+++: `date +'%Y''%m%d_%H:%M:%S'` $PROGNAME: $1\e[0m" | tee -a log/$logfn
}

fail(){
        echo -e "\e[1;31m+++FAIL+++: `date +'%Y''%m%d_%H:%M:%S'` $PROGNAME: $1\e[0m" | tee -a log/$logfn
}

test1(){
local uuid=`uuidgen | cut -d- -f1`
info "Creating Test Ado 1"

cat <<EOD | ac_bl - >/dev/null 2>/dev/null
TRANS
OBJECT ADO ADD symbol="C0.T$uuid" longname="ADO T1" template="C0_I_T006_LSA"
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0#I0149" value="Y" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0_IS093" value="$uuid" status=1
EOD

	success "Creating Test Ado 1 C0.T$uuid."
	info "C0_IS093 equals $uuid"

	info "Waiting for publisher to update ACX"
	sleep 10

info "Executing AC+ Ado Browse"
local sym2=`cat <<EOF | ac_evalform -f -
local val = ado_browse("attribute('C0_IS093') = '$uuid'");
print(str(val));
EOF`
local sym3=`echo $sym2 | sed -e "s/'//g" | sed -e 's/\[//g' | sed -e 's/\]//g'`

	info "Executing Acx Ado Browse"
	local sym1=`mvn exec:java -Dexec.mainClass="com.acx.AdoBrowseSingle" -Dexec.args="$uuid" | grep C0`

	info "ACX returns $sym1"
	info "AC+ returns $sym3"

	if [ $sym1 == $sym3 ]
	then
		pass "Test 1 : ACX return equals AC+ return"
	else
		fail "Test 1 : ACX return does not equal AC+ return."
	fi
}

test2(){
local uuid=`uuidgen | cut -d- -f1`
local uuid1=`uuidgen | cut -d- -f1`
local uuid2=`uuidgen | cut -d- -f1`

info "Creating Test Ado 2"

cat <<EOD | ac_bl - >/dev/null 2>/dev/null
TRANS
OBJECT ADO ADD symbol="C0.T$uuid" longname="ADO T1" template="C0_I_T006_LSA"
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0#I0149" value="Y" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0_IS093" value="$uuid1" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0_I0111" value="$uuid2" status=1
EOD
	success "Creating Test Ado 2 C0.T$uuid."
	info "C0_IS093 equals $uuid1"
	info "C0_I0111 equals $uuid2"

	info "Waiting for publisher to update ACX"
	sleep 10

info "Executing AC+ Ado Browse"
local sym2=`cat <<EOF | ac_evalform -f -
local val = ado_browse("attribute('C0_IS093') = '$uuid1' and attribute('C0_I0111') = '$uuid2'");
print(str(val));
EOF`
local sym3=`echo $sym2 | sed -e "s/'//g" | sed -e 's/\[//g' | sed -e 's/\]//g'`

	info "Executing Acx Ado Browse"
	local sym1=`mvn exec:java -Dexec.mainClass="com.acx.AdoBrowseMultipleAND" -Dexec.args="$uuid1 $uuid2" | grep C0`

	info "ACX returns $sym1"
	info "AC+ returns $sym3"

	if [ $sym1 == $sym3 ]
	then
		pass "Test 2 : ACX return equals AC+ return"
	else
		fail "Test 2 : ACX return does not equal AC+ return."
	fi
}

test3(){
local uuid=`uuidgen | cut -d- -f1`
local uuid1=`uuidgen | cut -d- -f1`
local uuid2=`uuidgen | cut -d- -f1`

info "Creating Test Ado 3"
info "uuid1 equals $uuid1"
info "uuid2 equals $uuid2"

cat <<EOD | ac_bl - >/dev/null 2>/dev/null
TRANS
OBJECT ADO ADD symbol="C0.T$uuid" longname="ADO T1" template="C0_I_T006_LSA"
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0#I0149" value="Y" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0_IS093" value="$uuid1" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0_I0111" value="$uuid2" status=1
EOD
	success "Creating Test Ado 3 C0.T$uuid."
	info "C0_IS093 equals $uuid1"
	info "C0_I0111 equals $uuid2"

	info "Waiting for publisher to update ACX"
	sleep 10

info "Executing AC+ Ado Browse"
local sym2=`cat <<EOF | ac_evalform -f -
local val = ado_browse("attribute('C0_IS093') = '$uuid1' or attribute('C0_I0111') = 'uuid3'");
print(str(val));
EOF`
local sym3=`echo $sym2 | sed -e "s/'//g" | sed -e 's/\[//g' | sed -e 's/\]//g'`

	info "Executing Acx Ado Browse"
	local sym1=`mvn exec:java -Dexec.mainClass="com.acx.AdoBrowseMultipleOR" -Dexec.args="$uuid1 uuid3" | grep C0`

	info "ACX returns $sym1"
	info "AC+ returns $sym3"

	if [ $sym1 == $sym3 ]
	then
		pass "Test 3 : ACX return equals AC+ return."
	else
		fail "Test 3 : ACX return does not equal AC+ return."
	fi
}

test4(){
local uuid=`uuidgen | cut -d- -f1`
info "Creating Test Ado 4"
cat <<EOD | ac_bl - >/dev/null 2>/dev/null
TRANS
OBJECT ADO ADD symbol="C0.T$uuid" longname="ADO T1" template="C0_I_T006_LSA"
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0#I0149" value="Y" status=1
EOD

	success "Creating Test Ado 4 C0.T$uuid."
	info "C0_IS093 equals $uuid"

	info "Waiting for publisher to update ACX"
	sleep 10

	info "Executing AC+ Ado Browse"
	local sym2=`echo "select symbol from fundmstr where symbol like '%T$uuid'" | ac_bl -qs -`

	info "Executing Acx Ado Browse"
	local sym1=`mvn exec:java -Dexec.mainClass="com.acx.AdoBrowseSingleLIKE" -Dexec.args="%T$uuid" | grep C0`

	info "ACX returns $sym1"
	info "AC+ returns $sym2"

	if [ $sym1 == $sym2 ]
	then
		pass "Test 4 : ACX return equals AC+ return"
	else
		fail "Test 4 : ACX return does not equal AC+ return."
	fi
}

test5(){
local uuid1=`uuidgen | cut -d- -f1`
local uuid2=`uuidgen | cut -d- -f1`
local uuid3=`uuidgen | cut -d- -f1`
local uuid4=`uuidgen | cut -d- -f1`
local uuid5=`uuidgen | cut -d- -f1`

info "Creating Test Ados 5.1 and 5.2"
cat <<EOD | ac_bl - >/dev/null 2>/dev/null
TRANS
OBJECT ADO ADD symbol="C0.T$uuid1" longname="ADO T1" template="C0_I_T006_LSA"
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid1" attrid="C0#I0149" value="Y" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid1" attrid="C0_IS094" value="$uuid3" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid1" attrid="C0_I0111" value="$uuid4" status=1
TRANS
OBJECT ADO ADD symbol="C0.T$uuid2" longname="ADO T1" template="C0_I_T006_LSA"
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid2" attrid="C0#I0149" value="Y" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid2" attrid="C0_IS094" value="$uuid3" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid2" attrid="C0_I0111" value="$uuid5" status=1
EOD

	success "Creating Test Ado 5 C0.T$uuid1."
	success "Creating Test Ado 5 C0.T$uuid2."
	info "attrid1 equals $uuid3"
	info "attrid2 equals $uuid4"
	info "attrid3 equals $uuid5"

	info "Waiting for publisher to update ACX"
	sleep 10

info "Executing AC+ Ado Browse"
local sym2=`cat <<EOF | ac_evalform -f -
local val = ado_browse("attribute('C0_IS094') = '$uuid3' and attribute('C0_I0111') != '$uuid4'");
print(str(val));
EOF`
local sym3=`echo $sym2 | sed -e "s/'//g" | sed -e 's/\[//g' | sed -e 's/\]//g'`

	info "Executing Acx Ado Browse"
	local sym1=`mvn exec:java -Dexec.mainClass="com.acx.AdoBrowseSingleNOT" -Dexec.args="$uuid3 $uuid4" | grep C0`

	info "ACX returns $sym1"
	info "AC+ returns $sym3"

	if [ $sym1 == $sym3 ]
	then
		pass "Test 5 : ACX return equals AC+ return"
	else
		fail "Test 5 : ACX return does not equal AC+ return."
	fi
}

test6(){
local uuid=`uuidgen | cut -d- -f1`
info "Creating Test Ado 6"
cat <<EOD | ac_bl - >/dev/null 2>/dev/null
TRANS
OBJECT ADO ADD symbol="C0.T$uuid" longname="ADO T1" template="C0_I_T006_LSA"
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0#I0149" value="Y" status=1
EOD

	success "Creating Test Ado 6 C0.T$uuid."

	info "Waiting for publisher to update ACX"
	sleep 10

info "Executing AC+ Ado Browse"
local sym2=`cat <<EOF | ac_evalform -f -
local val = attribute("C0.T$uuid","C0#I0149");
print(str(val));
EOF`
local sym3=`echo $sym2 | sed -e "s/'//g" | sed -e 's/\[//g' | sed -e 's/\]//g'`

	info "Executing Acx Ado Browse"
	local sym1=`mvn exec:java -Dexec.mainClass="com.acx.StaticAttributes" -Dexec.args="C0.T$uuid" | grep C0#I0149`

	local sym4=${sym1//[[:blank:]]/}
	local sym5=`echo $sym4 | grep C0#I0149 | grep com.assetcontrol.acx.model.values.valuesimpl.ValueStringImpl | grep content:Y`

	if [ $sym5 ]
	then
	        if [ $sym3 == 'Y' ]
        	then
                	pass "ACX returns the expected value"
	        else
        	        fail "ACX does not return the expected value"
        	fi
	else
        	fail "ACX does not return the expected value"
	fi
}

test7(){
local uuid=`uuidgen | cut -d- -f1`
info "Creating Test Ado 7"
cat <<EOD | ac_bl -
TRANS
OBJECT ADO ADD symbol="C0.T$uuid" longname="ADO T1" template="C0_I_T006_LSA"
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0#I0149" value="Y" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0_IS141" value=1.0 status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0#I0145" value=20181004 status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0#I0011" value=1 status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0#I0007" value="5Z" status=1
TRANS
OBJECT ADO_ATTRIBUTE ADD symbol="C0.T$uuid" attrid="C0#COMP_0100" value=!"[['C0.T$uuid','CONSOLIDATION_C0','CONSOLIDATION_C0_DERIVED']]" status=1
EOD

	success "Creating Test Ado 7 C0.T$uuid."

	info "Waiting for publisher to update ACX"
	sleep 20

info "Executing AC+ Static Attribute Queries"
local v1=`cat <<EOF | ac_evalform -f -
local val = attribute("C0.T$uuid","C0#I0011");
print(str(val));
EOF`
local v2=`echo $sym2 | sed -e "s/'//g" | sed -e 's/\[//g' | sed -e 's/\]//g'`

local v3=`cat <<EOF | ac_evalform -f -
local val = attribute("C0.T$uuid","C0#I0145");
print(str(val));
EOF`
local v4=`echo $sym2 | sed -e "s/'//g" | sed -e 's/\[//g' | sed -e 's/\]//g'`

local v5=`cat <<EOF | ac_evalform -f -
local val = attribute("C0.T$uuid","C0#I0007");
print(str(val));
EOF`
local v6=`echo $sym2 | sed -e "s/'//g" | sed -e 's/\[//g' | sed -e 's/\]//g'`

local v7=`cat <<EOF | ac_evalform -f -
local val = attribute("C0.T$uuid","C0#COMP_0100");
print(str(val));
EOF`
local v8=`echo $sym2 | sed -e "s/'//g" | sed -e 's/\[//g' | sed -e 's/\]//g'`

local v9=`cat <<EOF | ac_evalform -f -
local val = attribute("C0.T$uuid","C0_IS141");
print(str(val));
EOF`
local vv10=`echo $sym2 | sed -e "s/'//g" | sed -e 's/\[//g' | sed -e 's/\]//g'`

	info "C0#I0011 attribute equals $v1"
	info "C0#I0145 attribute equals $v3"
	info "C0#I0007 attribute equals $v5"
	info "C0#COMP_0100 attribute equals $v7"
	info "C0_IS141 attribute equals $v9"

	info "Executing Acx Ado Browse"
	local var11=`mvn exec:java -Dexec.mainClass="com.acx.StaticAttributes" -Dexec.args="C0.T$uuid C0#I0011" | grep C0`
	local var12=`mvn exec:java -Dexec.mainClass="com.acx.StaticAttributes" -Dexec.args="C0.T$uuid C0_IS141" | grep C0`
	local var13=`mvn exec:java -Dexec.mainClass="com.acx.StaticAttributes" -Dexec.args="C0.T$uuid C0#I0145" | grep C0`
	local var14=`mvn exec:java -Dexec.mainClass="com.acx.StaticAttributes" -Dexec.args="C0.T$uuid C0#I0007" | grep C0`
	local var15=`mvn exec:java -Dexec.mainClass="com.acx.StaticAttributes" -Dexec.args="C0.T$uuid C0#COMP_0100" | grep C0`

	info "C0#I0011 attribute equals $var11"
	info "C0_IS141 attribute equals $var12"
	info "C0#I0012 attribute equals $var13"
	info "C0#I0007 attribute equals $var14"
	info "C0#COMP_0100 attribute equals $var15"

}

mvn --version >/dev/null 2>/dev/null
success "Verifying maven installation"

info "Building java project"
mvn compile >/dev/null 2>/dev/null
success "Building java project."

info "\e[0;33mTest 1 : AdoBrowse with single attribute\e[0m"
	test1

info "\e[0;33mTest 2 : AdoBrowse with multiple attributes, Logical Test AND\e[0m"
	test2

info "\e[0;33mTest 3 : AdoBrowse with multiple attributes, Logical Test OR\e[0m"
	test3

info "\e[0;33mTest 4 : AdoBrowse with single attribute, wildcard\e[0m"
	test4

info "\e[0;33mTest 5 : AdoBrowse with single attribute, Logical Test NOT\e[0m"
	test5

info "\e[0;33mTest 6 : Retrieve all static attributes for an ado\e[0m"
	test6

info "\e[0;33mTest 7 : Retrieve static attributes of differing datatypes for an ado\e[0m"
	test7

info "\e[0;33mTest 8 : Retrieve timeseries for an ado for a particular asOn asOf\e[0m"

