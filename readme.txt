1. execute test.sh

2. test.sh compiles the java project and 
runs each test sequentially.

3. test Sequence : (i) adoBrowse logical tests
                   (ii) static attribute query tests for different data types.
                   (iii) timeseries query tests for variable asOf asOn.

4. each test does the following:- (i) creates a test ado/s in AC Server
				  (ii) waits for publisher to update ACX
				  (iii) queries the ADO on AC Server with Formula Engine.
				  (iv) queries the ADO on ACX with Java API.
				  (v) compares the two results.
				  (vi) if the two results match, test passes.
		
5. logging is generated in "log" directory with a file generated at the start time of an execution of test.sh.

6. example output is as follows.

+++INFO+++: 20181023_09:56:19 test.sh: Success: Verifying maven installation
+++INFO+++: 20181023_09:56:19 test.sh: Building java project
+++INFO+++: 20181023_09:56:25 test.sh: Success: Building java project.
+++INFO+++: 20181023_09:56:25 test.sh: Test 1 : AdoBrowse with single attribute
+++INFO+++: 20181023_09:56:25 test.sh: Creating Test Ado 1
+++INFO+++: 20181023_09:56:25 test.sh: Success: Creating Test Ado 1 C0.Tb7c7b7af.
+++INFO+++: 20181023_09:56:25 test.sh: C0_IS093 equals b7c7b7af
+++INFO+++: 20181023_09:56:25 test.sh: Waiting for publisher to update ACX
+++INFO+++: 20181023_09:56:35 test.sh: Executing AC+ Ado Browse
+++WARNING+++ 20181023_09:56:36 FE (20100422): ['C0.Tb7c7b7af']
+++INFO+++: 20181023_09:56:36 test.sh: Executing Acx Ado Browse
+++INFO+++: 20181023_09:56:55 test.sh: ACX returns C0.Tb7c7b7af
+++INFO+++: 20181023_09:56:55 test.sh: AC+ returns C0.Tb7c7b7af
+++PASS+++: 20181023_09:56:55 test.sh: Test 1 : ACX return equals AC+ return
